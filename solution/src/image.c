#include "image.h"
#include <stdlib.h>

void struct_image_data_free (struct image image) {
    if (image.data!=NULL) free(image.data);
}

struct image struct_image_init(void) {
    return (struct image) {0};
}
