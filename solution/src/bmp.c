#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


// (3 * width) % 4 == width%4
static uint32_t padding_value(uint32_t width) {
    return  width % 4;
}

//initializing bmp_header with fields to avoid corruption
static struct bmp_header init_new_header (uint32_t image_size_with_padding, uint32_t height, uint32_t width) {
    struct bmp_header bmp_hdr = {0};
    bmp_hdr.bfType = 0x4D42;
    bmp_hdr.bfileSize = sizeof(struct bmp_header) + image_size_with_padding;
    bmp_hdr.bfReserved = 0;;
    bmp_hdr.bOffBits = sizeof(struct bmp_header);
    bmp_hdr.biSize = 40;
    bmp_hdr.biWidth = width;
    bmp_hdr.biHeight = height;
    bmp_hdr.biPlanes = 1;
    bmp_hdr.biBitCount = 24;
    bmp_hdr.biCompression = 0;
    bmp_hdr.biSizeImage = image_size_with_padding;
    bmp_hdr.biXPelsPerMeter = 0;
    bmp_hdr.biYPelsPerMeter = 0;
    bmp_hdr.biClrUsed = 0;
    bmp_hdr.biClrImportant = 0;
    return bmp_hdr;

}


static enum read_status struct_image_data_fill(FILE* in, struct pixel* data, uint32_t height, uint32_t width, uint32_t padding) {
    for (uint32_t i = 0; i < height; i++) {
            fread(data + i*width, sizeof(struct pixel), width, in);
            fseek(in, padding, SEEK_CUR);
        if (ferror(in)) return READ_IMAGE_ERROR;
    }
    return READ_OK;

}

static enum write_status struct_image_data_empty(FILE* out, struct pixel* data, uint32_t height, uint32_t width,  uint32_t padding) {
    const struct pixel pad = {0,0, 0};
    for (uint32_t i = 0; i < height; i++) {
        fwrite(data + i*width, sizeof(struct pixel), width, out);
        fwrite(&pad, 1, padding, out);

       if (ferror(out)) return  WRITE_IMAGE_ERROR;
    }
    return WRITE_OK;
}



//reading from bmp file
enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header bmp_image_header;
    if (fread(&bmp_image_header, sizeof(struct bmp_header), 1, in)) {
        img->width = bmp_image_header.biWidth;
        img->height = bmp_image_header.biHeight;
        img->data = malloc(sizeof(struct pixel) * img->width * img->height);
        uint32_t const image_padding = padding_value(img->width);

        //placing file-reading pointer to the beginning of the image
        fseek(in, bmp_image_header.bOffBits, SEEK_SET);

        return struct_image_data_fill(in, img->data, img->height, img->width, image_padding);
    } else return READ_HEADER_ERROR;
}

//writing to the bmp file
enum write_status to_bmp(FILE *out, struct image const *img) {
    uint32_t const image_padding = padding_value(img->width);
    uint32_t const image_size_with_padding = img->height*(image_padding+3*img->width);

    struct bmp_header bmp_image_header = init_new_header(image_size_with_padding, img->height, img->width);

    if(fwrite(&bmp_image_header, sizeof(struct bmp_header), 1, out)) {

        return struct_image_data_empty(out, img->data, img->height, img->width, image_padding);

    } return  WRITE_HEADER_ERROR;

}
