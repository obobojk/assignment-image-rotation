#include <bmp.h>
#include <image.h>
#include <image_rotation.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc!= 3) {
        printf("Invalid number of args. Should be 'command file-in file-out' \n");
    } else {
        FILE * file_in = fopen(argv[1], "r");
        FILE * file_out = fopen(argv[2], "w");

        if (file_in!=NULL) {
            struct image img = struct_image_init();
            from_bmp(file_in, &img);

            struct image rotated_image = rotate(img);
            to_bmp(file_out, &rotated_image);

            struct_image_data_free(img); struct_image_data_free(rotated_image);

            fclose(file_in); fclose(file_out);

        } else { printf("Null in argument.\n");}
    }

    return 0;
}
