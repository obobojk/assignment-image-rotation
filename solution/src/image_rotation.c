#include "image_rotation.h"
#include <stdlib.h>

//rotating an image by 90 degrees counter clock wise

static uint64_t get_rotated_index(uint64_t i, uint64_t j, uint64_t height) {
    return (height - i - 1) + j*height;
}
static uint64_t get_index(uint64_t i, uint64_t j, uint64_t width) {
    return j + i*width;
}

struct image rotate( struct image const source ) {
    uint64_t img_height = source.height;
    uint64_t img_width = source.width;
    struct pixel *img_pixels = source.data;
    struct pixel *rotated_pixels = malloc(sizeof(struct pixel)*img_height*img_width);

    for (uint64_t i = 0; i < img_height; i++)
        for (uint64_t j = 0; j < img_width; j++) {
            rotated_pixels[get_rotated_index(i,j,img_height)] = img_pixels[get_index(i,j,img_width)];
        }

    struct image rotated_image = struct_image_init();
    rotated_image.height = img_width;
    rotated_image.width = img_height;
    rotated_image.data = rotated_pixels;
    return rotated_image;
}
