#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct pixel { uint8_t r, g, b; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void struct_image_data_free(struct image image);
struct image struct_image_init(void);
#endif
